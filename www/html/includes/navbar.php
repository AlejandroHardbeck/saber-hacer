<?php include 'mdb.php'; ?>

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta charset="utf-8">
<link rel="icon" type="imge/png" href="../img/logo.png">

<nav class="navbar navbar-expand-lg  fixed-top" style="background-color:  rgb(255, 255, 255, 0.5);  ">

	<a class="navbar-brand" >
		<img src="../Img/logo.png" alt="ALMACÉN" style="width:40px;">
	</a>
	<a class="navbar-brand" href="../" style=" color: black;" >Inicio</a>

	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span> Menú
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a class="nav-link" href="../ingresarmaterial/" style=" color: black;" >Ingresar Material</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="../materialesingresados/" style=" color: black;">Materiales ingresados</a>
			</li>
		</ul>
		<form class="form-inline my-2 my-lg-0">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a href="../action/cerrar.php" class="nav-link" style=" color: black;" >Cerrar Sesión</a>
				</li>
				<li class="	nav-item">	
					<a class="nav-link" href="../registro/" style=" color: black;">Registrar</a>
				</li>
				<li class="	nav-item">
					<a href="../action/logout.php" class="btn btn-danger">Terminar Turno</a>
				</li>
			</ul>
		</form>
	</div>
</nav>